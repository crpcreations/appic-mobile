package com.crp.chintanappic.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.crp.chintanappic.Adapter.RecyclerrAdapter;
import com.crp.chintanappic.Model.DataModel;
import com.crp.chintanappic.Model.TransacModel;
import com.crp.chintanappic.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

public class MainActivity extends AppCompatActivity {

    List<DataModel> jsonValues = new ArrayList<>();

    String jsonString = "[\n" +
            "    {\n" +
            "      \"Mid\": 1,\n" +
            "      \"Tid\": 123456797,\n" +
            "      \"amount\": 12.32,\n" +
            "      \"narration\": 11234684654\n" +
            "    },\n" +
            "    {\n" +
            "      \"Mid\": 2,\n" +
            "      \"Tid\": 123456794,\n" +
            "      \"amount\": 12.32,\n" +
            "      \"narration\": 11234684654\n" +
            "    },\n" +
            "    {\n" +
            "      \"Mid\": 3,\n" +
            "      \"Tid\": 123456791,\n" +
            "      \"amount\": 12.32,\n" +
            "      \"narration\": 11234684654\n" +
            "    },\n" +
            "    {\n" +
            "      \"Mid\": 3,\n" +
            "      \"Tid\": 123456791,\n" +
            "      \"amount\": 12.32,\n" +
            "      \"narration\": 11234684654\n" +
            "    },\n" +
            "    {\n" +
            "      \"Mid\": 1,\n" +
            "      \"Tid\": 123456797,\n" +
            "      \"amount\": 12.32,\n" +
            "      \"narration\": 11234684654\n" +
            "    },\n" +
            "    {\n" +
            "      \"Mid\": 2,\n" +
            "      \"Tid\": 123456794,\n" +
            "      \"amount\": 12.32,\n" +
            "      \"narration\": 11234684654\n" +
            "    },\n" +
            "    {\n" +
            "      \"Mid\": 1,\n" +
            "      \"Tid\": 123456797,\n" +
            "      \"amount\": 12.32,\n" +
            "      \"narration\": 11234684654\n" +
            "    },\n" +
            "    {\n" +
            "      \"Mid\": 3,\n" +
            "      \"Tid\": 123456791,\n" +
            "      \"amount\": 12.32,\n" +
            "      \"narration\": 11234684654\n" +
            "    },\n" +
            "    {\n" +
            "      \"Mid\": 1,\n" +
            "      \"Tid\": 123456795,\n" +
            "      \"amount\": 12.32,\n" +
            "      \"narration\": 11234684654\n" +
            "    },\n" +
            "    {\n" +
            "      \"Mid\": 1,\n" +
            "      \"Tid\": 123456795,\n" +
            "      \"amount\": 12.32,\n" +
            "      \"narration\": 11234684654\n" +
            "    },\n" +
            "    {\n" +
            "      \"Mid\": 3,\n" +
            "      \"Tid\": 123456791,\n" +
            "      \"amount\": 12.32,\n" +
            "      \"narration\": 11234684654\n" +
            "    }\n" +
            "  ]";

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
       /* try {
            jsonToList();
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        newMet();
    }

/*
    void jsonToList() throws JSONException {
        JSONArray myJsonArray = new JSONArray(jsonString);
        for (int i = 0; i < myJsonArray.length(); i++) {
            DataModel dataModel = new DataModel();
            JSONObject jsonObject = myJsonArray.getJSONObject(i);
            dataModel.setAmount(Double.valueOf(jsonObject.optString("amount")));
            dataModel.setMid(Integer.valueOf(jsonObject.optString("Mid")));
            dataModel.setTid(Integer.valueOf(jsonObject.optString("Tid")));
            dataModel.setNarration(jsonObject.optString("narration"));
            jsonValues.add(dataModel);
        }

        Collections.sort(jsonValues, (s1, s2) -> Integer.compare(s2.getMid(), s1.getMid()));
        Collections.reverse(jsonValues);
      //  setList();
    }
*/

    void setList() {
       // RecyclerrAdapter recyclerrAdapter = new RecyclerrAdapter(jsonValues, MainActivity.this);
        ///recyclerView.setAdapter(recyclerrAdapter);
    }

    void newMet(){
        TreeMap map = new TreeMap();



        try {
            JSONArray jsonArray = new JSONArray(jsonString);

            for (int i = 0; i < jsonArray.length(); i++) {

                ArrayList dataArr;
                String mid = jsonArray.getJSONObject(i).optString("Mid");

                if (map.containsKey(mid)) {
                    dataArr = (ArrayList) map.get(mid);
                } else {
                    dataArr = new ArrayList();
                }
                TreeMap taskList = new TreeMap();

                taskList.put("Tid", jsonArray.getJSONObject(i).optString("Tid"));
                taskList.put("amount", jsonArray.getJSONObject(i).optString("amount"));
                taskList.put("narration", jsonArray.getJSONObject(i).optString("narration"));
                taskList.put("type", "data");


                dataArr.add(taskList);
                map.put(mid, dataArr);
            }


            JSONArray finalArray = new JSONArray();
            for (Object o1 : map.keySet()) {
                String mid = (String) o1;
                JSONObject dateObj = new JSONObject();
                JSONObject dataObj;
                ArrayList data = (ArrayList) map.get(mid);

                dateObj.put("Mid", mid);
                dateObj.put("type", "mid");

                finalArray.put(dateObj);
                for (Object obj : data) {

                    dataObj = new JSONObject();
                    TreeMap tempMap = (TreeMap) obj;

                    for (Object o : tempMap.keySet()) {
                        String d = (String) o;
                        dataObj.put(d, tempMap.get(d));
                    }


                    finalArray.put(dataObj);
                }

            }

            String json =  new Gson().toJson(map);
            Log.e("This List", String.valueOf(json));
            Log.e("Final Array", String.valueOf(finalArray));
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            final DataModel[] calenderData = gson.fromJson(String.valueOf(finalArray), DataModel[].class);
            RecyclerrAdapter recyclerrAdapter = new RecyclerrAdapter(calenderData, MainActivity.this);
            recyclerView.setAdapter(recyclerrAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
