package com.crp.chintanappic.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crp.chintanappic.Model.DataModel;
import com.crp.chintanappic.R;

import java.util.List;

public class RecyclerrAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private DataModel[] dataModels;

    public RecyclerrAdapter(DataModel[] dataModels, Context context) {
        this.dataModels = dataModels;
        this.context = context;
    }

    private Context context;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;

        switch (i) {
            case 0:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mainlayout, viewGroup, false);
                return new MidLayout(view);
            case 1:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sublayout, viewGroup, false);
                return new TidLayout(view);


        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        final DataModel data = dataModels[position];

        switch (data.getType()) {
            case "mid":
                return 0;
            case "data":
                return 1;

            default:
                return -1;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        DataModel numberPojo = dataModels[i];


        if (numberPojo.getType().equals("mid")) {
            ((MidLayout) holder).txtType.setText("Mid : " + numberPojo.getMid());


        } else if (numberPojo.getType().equals("data")) {

            ((TidLayout) holder).txtType.setText("Tid : " + numberPojo.getTid());
            ((TidLayout) holder).data_amt.setText("₹ " + numberPojo.getAmount());
            ((TidLayout) holder).data_narr.setText(numberPojo.getNarration());

            DataModel nu = dataModels[i - 1];
            if (numberPojo.getTid().equals(nu.getTid())) {
                ((TidLayout) holder).txtType.setVisibility(View.GONE);

            }

        }


      /*  ((TextView) recyclerrView.itemView.findViewById(R.id.data_amt)).setText("₹ " + numberPojo.getAmount());
        ((TextView) recyclerrView.itemView.findViewById(R.id.data_tid)).setText("Tid : " + numberPojo.getTid());
        ((TextView) recyclerrView.itemView.findViewById(R.id.data_narr)).setText("Na : " + numberPojo.getNarration());
        recyclerrView.itemView.findViewById(R.id.data_mid).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recyclerrView.itemView.findViewById(R.id.linearr).getVisibility() == View.VISIBLE)
                    recyclerrView.itemView.findViewById(R.id.linearr).setVisibility(View.GONE);
                else
                    recyclerrView.itemView.findViewById(R.id.linearr).setVisibility(View.VISIBLE);

            }
        });*/

    }

    public class MidLayout extends RecyclerView.ViewHolder {
        TextView txtType;

        MidLayout(View itemView) {
            super(itemView);
            txtType = itemView.findViewById(R.id.data_mid);
        }

    }

    public class TidLayout extends RecyclerView.ViewHolder {
        TextView txtType, data_amt, data_narr;

        TidLayout(View itemView) {
            super(itemView);
            txtType = itemView.findViewById(R.id.data_tid);
            data_amt = itemView.findViewById(R.id.data_amt);
            data_narr = itemView.findViewById(R.id.data_narr);

        }

    }

    @Override
    public int getItemCount() {
        return dataModels.length;
    }

}
