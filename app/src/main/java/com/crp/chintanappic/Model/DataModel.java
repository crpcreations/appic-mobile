package com.crp.chintanappic.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataModel {

    @SerializedName("Mid")
    @Expose
    private String mid;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("Tid")
    @Expose
    private String tid;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("narration")
    @Expose
    private String narration;

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

}
